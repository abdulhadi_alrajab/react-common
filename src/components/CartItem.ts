interface CartItem {
	id: number;
	name: string;
	price: number;
	image: string;
	count?: number;
}
const cartItemsManual: CartItem[] = [
	{
		id: 1,
		price: 200,
		count: 1,
		name: 'Freshing 1',
		image:
			'https://media.furchildpets.com/images/products/organic-chicken-necks--rhyajrchsd.jpg',
	},
	{
		id: 2,
		count: 1,
		price: 300,
		name: 'Freshing 2',
		image:
			'https://media.furchildpets.com/images/products/organic-chicken-necks--rhyajrchsd.jpg',
	},
	{
		id: 3,
		count: 1,
		price: 400,
		name: 'Freshing 3',
		image:
			'https://media.furchildpets.com/images/products/organic-chicken-necks--rhyajrchsd.jpg',
	},
	{
		id: 4,
		count: 1,
		price: 500,
		name: 'Freshing 4',
		image:
			'https://media.furchildpets.com/images/products/organic-chicken-necks--rhyajrchsd.jpg',
	},
	{
		id: 5,
		count: 1,
		price: 500,
		name: 'Freshing 4',
		image:
			'https://media.furchildpets.com/images/products/organic-chicken-necks--rhyajrchsd.jpg',
	},
	{
		id: 6,
		count: 1,
		price: 500,
		name: 'Freshing 4',
		image:
			'https://media.furchildpets.com/images/products/organic-chicken-necks--rhyajrchsd.jpg',
	},
	{
		id: 7,
		count: 1,
		price: 500,
		name: 'Freshing 4',
		image:
			'https://media.furchildpets.com/images/products/organic-chicken-necks--rhyajrchsd.jpg',
	},
	{
		id: 8,
		count: 1,
		price: 500,
		name: 'Freshing 4',
		image:
			'https://media.furchildpets.com/images/products/organic-chicken-necks--rhyajrchsd.jpg',
	},
	{
		id: 9,
		count: 1,
		price: 500,
		name: 'Freshing 4',
		image:
			'https://media.furchildpets.com/images/products/organic-chicken-necks--rhyajrchsd.jpg',
	},
];

export default cartItemsManual;
