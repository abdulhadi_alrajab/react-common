import React, {
    createContext, useState,
} from 'react';
import cartItems from '../../components/CartItem';

export interface CartItem {
    id: number;
    name: string;
    price: number;
    image: string;
    count?: number;
}

interface CartProviderProps {
    children: React.ReactNode
}

interface CartContextType {
    listOfItems: CartItem[];
    count: number;
    isMenuOpen: boolean;
    addItem: (item: CartItem) => void;
    removeItem: (id: number) => void;
    openMenu: () => void;
    closeMenu: () => void;
    isInCart: (id: number) => boolean;
    increaseCount: (id: number) => void;
    decreaseCount: (id: number) => void;
}

export const CartContext = createContext<CartContextType>({
    listOfItems: [],
    count: 0,
    isMenuOpen: false,
    addItem: () => { },
    removeItem: () => { },
    openMenu: () => { },
    closeMenu: () => { },
    isInCart: () => false,
    increaseCount: () => { },
    decreaseCount: () => { },
});
export default function CartProvider({ children }: CartProviderProps) {
    const [listOfItems, setListOfItems] = useState<CartItem[]>(cartItems);
    const [count, setCount] = useState<number>(cartItems.length || 0);
    const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);

    const addItem = (item: CartItem) => {
        setListOfItems([...listOfItems, { ...item, count: 1 }]);
        setCount(count + 1);
    };

    const removeItem = (id: number) => {
        setListOfItems(listOfItems.filter((item) => item.id !== id));
        setCount(count - 1);
    };

    const openMenu = () => {
        setIsMenuOpen(true);
    };

    const closeMenu = () => {
        setIsMenuOpen(false);
    };

    const isInCart = (id: number) => listOfItems.some((item) => item.id === id);

    const increaseCount = (id: number) => {
        setListOfItems(
            listOfItems.map((i) => (i.id === id ? { ...i, count: i.count! + 1 } : i)),
        );
        setCount(count + 1);
    };

    const decreaseCount = (id: number) => {
        const item = listOfItems.find((i) => i.id === id);
        if (item && item.count && item.count > 1) {
            setListOfItems(
                listOfItems.map((i) => (i.id === id ? { ...i, count: i.count! - 1 } : i)),
            );
            setCount(count - 1);
        }
    };

    return (
        <CartContext.Provider
            value={{
                listOfItems,
                count,
                isMenuOpen,
                addItem,
                removeItem,
                openMenu,
                closeMenu,
                isInCart,
                decreaseCount,
                increaseCount,
            }}
        >
            {children}
        </CartContext.Provider>
    );
}
