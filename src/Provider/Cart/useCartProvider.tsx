import { useContext } from 'react';
import { CartContext } from './CartProvider';

export default function useCartContext() {
    return useContext(CartContext);
}
